let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101" ," Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", " Natural Sciences 402"]
}

const introduce = () => {

	const {name, birthday, age, isEnrolled, classes} = student1;

	console.log(`Hi I'm ${name}. I am ${age} years old.\nI study the following courses: ${classes}.`);

}

introduce();

/*function introduce(student){

	//Note: You can destructure objects inside functions.

	console.log("Hi! " + "I'm " + student.name + " ." + " I am " + student.ages + " years old.");
	console.log("I study the following courses + " classes);
}*/





const getCube = (num) => {
	console.log(num**3);
};

let cube = getCube(3);

/*function getCube(num){

	console.log(Math.pow(num,3));

}*/





let numArr = [15,16,32,21,21,2];

const [num1, num2, num3, num4, num5, num6] = numArr;

numArr.forEach((num) =>{console.log(num);})

/*
numArr.forEach(function(num){
	console.log(num);
})*/


let numSquared = numArr.map((num) =>{return num ** 2})

console.log(numSquared);




/*
let numSquared = numArr.map(function(num){

	return num ** 2;

})*/





class Dog {
	constructor(name, breed, age){
		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	};
};


let dog1 = new Dog("Hershey","Shih tzu",5);
console.log(dog1);

let dog2 = new Dog("Zoe","Beagle",6);
console.log(dog2);